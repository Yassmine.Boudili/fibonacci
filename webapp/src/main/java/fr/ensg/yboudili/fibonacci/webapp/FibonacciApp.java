package fr.ensg.yboudili.fibonacci.webapp;

import fr.ensg.yboudili.fibonacci.core.Fibonacci;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class FibonacciApp {
    public static void main( String[] args ) {
        org.springframework.boot.SpringApplication.run(FibonacciApp.class, args);
    }
    @RestController
    public static class FibonacciController {

        @GetMapping("/fibonacci/{n}")
        public long calculate(@PathVariable long n) {
            return Fibonacci.calculate(n);
        }
    }
}

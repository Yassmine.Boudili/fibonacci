package fr.ensg.yboudili.fibonacci.core;

/**
 * Fibonacci is the class used to calculate Fibonacci numbers
 * @author Yassmine BOUDILI yasmine.boudili@ensg.eu
 */
public class Fibonacci {

    /**
     * calculate is the method that calculates the Fibonacci number for a given rank
     * @param n the rank at the Fibonacci sequence
     * @return the Fibonacci number corresponding to the input rank
     */
    public static long calculate(long n){

        if (n <= 0)  return 0;
        if (n < 2)  return 1;
        return calculate(n - 1) + calculate(n - 2);

    }
}

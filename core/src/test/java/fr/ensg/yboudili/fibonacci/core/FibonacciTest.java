package fr.ensg.yboudili.fibonacci.core;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FibonacciTest {

    @Test
    public void calculateTest() {
        Fibonacci tester = new Fibonacci();
        assertEquals(0, tester.calculate(0));
        assertEquals(1, tester.calculate(1));
        assertEquals(0, tester.calculate(-1));
        assertEquals(233, tester.calculate(13));
        assertEquals(0, tester.calculate(Long.MIN_VALUE));
    }
}